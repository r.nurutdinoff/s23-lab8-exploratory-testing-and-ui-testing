# Lab 8 - Exploratory testing and UI

I chose **www.npmjs.com**

I implemented test for **first** case

1) Validating that there is command for downloading the @testing-library/react on the package page.
- open main page
- typing @testing-library/react
- press enter
- click on first entry in the list of options
- find command on the page of package

2) Validating that there is form for registration that consists of 3 fields: password, login, email
- open main page
- click on sign up button
- check whether password, login, email got rendered on the page

3) Validating whether link on documentation page is rendered correctly
- open main page
- validate whether header got rendered
- make sure that docs link is present
- click on link
- validate whether url got changed on https://docs.npmjs.com/