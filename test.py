from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options

options = Options()
options.headless = True

driver = webdriver.Remote(
        command_executor='http://selenium__standalone-firefox:4444/wd/hub',
        options=options
)

driver.implicitly_wait(10) # seconds

driver.get("http://www.npmjs.com")
print("Page title is: "+driver.title)
assert "npm" in driver.title

search = driver.find_element(By.NAME, "q")
assert search.is_displayed()
search.clear()
search.send_keys("f,lqw kp2ep m f ")
search.send_keys(Keys.ENTER)


packagesCounter = driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/div[1]/h2')
assert packagesCounter.text == '0 packages found'

search = driver.find_element(By.NAME, "q")
assert search.is_displayed()
search.clear()
search.send_keys("@testing-library/react")
search.send_keys(Keys.ENTER)

linkToLibrary = driver.find_element(By.LINK_TEXT, '@testing-library/react')
linkToLibrary.click()

installButton = driver.find_element(By.XPATH, '/html/body/div/div/div[2]/main/div/div[3]/p/code/span')
assert installButton.text == 'npm i @testing-library/react'


# elem = driver.find_element(By.NAME, "q")
# assert elem.get_attribute("value") == "SQR course is the best"
# assert "SQR course is the best" in driver.title

# driver.close()
